-- LUALOCALS < ---------------------------------------------------------
local ipairs, math, minetest, nodecore, pairs, string, table, tonumber,
      vector
    = ipairs, math, minetest, nodecore, pairs, string, table, tonumber,
      vector
local math_floor, math_random, string_format, table_concat, table_sort
    = math.floor, math.random, string.format, table.concat, table.sort
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local modstore = minetest.get_mod_storage()

-- Settings are late-bound and can be changed at runtime, but some (e.g. block limit)
-- may take until the next 60 second cycle to take effect.

local function get_block_limit()
	-- The maximum number of mapblocks that can be forceloaded by this mod.
	return math_floor(tonumber(minetest.settings:get(modname .. "_blocks")) or 256)
end
local function get_level_max()
	-- Artificial level limit to disallow level 3 or even level 2 loaders.
	return math_floor(tonumber(minetest.settings:get(modname .. "_level_max")) or 3)
end
local function get_cost_scale()
	-- Adjust the level that results from a given power level.
	-- Adjust of 0 is default, 1 makes everything 1 level easier, -1 makes
	-- everything 1 level harder. Fractional values are allowed.
	return 0.25 ^ (tonumber(minetest.settings:get(modname .. "_level_adjust")) or 0)
end

local anchorname = modname .. ":anchor"

------------------------------------------------------------------------
-- OPTICAL STIMULATION

local db
do
	local s = modstore:get_string("db")
	db = s and s ~= "" and minetest.deserialize(s) or {}
end

local signaldirs = {
	{x = -1, y = 0, z = 0},
	{x = 1, y = 0, z = 0},
	{x = 0, y = 0, z = -1},
	{x = 0, y = 0, z = 1},
}

local levels = {
	{qty = 30, around = 0, volume = 1, facearea = 1},
	{qty = 120, around = 1, volume = 27, facearea = 9},
	{qty = 480, around = 2, volume = 125, facearea = 25},
}

local function nav(dict, key)
	local found = dict[key]
	if found then return found end
	found = {}
	dict[key] = found
	return found
end

local function setlevel(pos, total, old, new)
	nodecore.log("action", string_format("%s at %s power %d level %d -> %d",
			anchorname, minetest.pos_to_string(pos), total, old, new))
	return new
end

local function statecheck(pos, recv, key)
	if nodecore.stasis then return end
	local loaded = minetest.get_node_or_nil(pos)
	if loaded and (loaded.name ~= anchorname) then
		db[key] = nil
		return {total = 0}
	end

	key = key or minetest.pos_to_string(pos)
	local entry = nav(db, key)

	if not loaded then
		entry.ts = nodecore.gametime
		return entry
	end

	local last = entry.ts or 0
	local rawshift = math_floor((nodecore.gametime - last) / 2)
	local shift = (rawshift >= 5) and 5 or rawshift

	local total
	if shift > 0 or recv then
		total = 0
		for dirn, dirv in ipairs(signaldirs) do
			local dirstate = nav(entry, dirn)
			if shift > 0 then
				for i = 5, shift + 1, -1 do
					dirstate[i] = dirstate[i - shift]
				end
				for i = 1, shift do
					dirstate[i] = 0
				end
			end
			if recv then
				local ns = not recv(dirv)
				if ns ~= dirstate.s then
					dirstate.s = ns
					dirstate[1] = dirstate[1] + 1
				end
			end
			for i = 1, 5 do total = total + (dirstate[i] or 0) end
		end
		entry.qty = total
	else
		total = entry.qty
	end
	entry.ts = last + rawshift * 2

	local level = entry.lv or 0
	if entry.lpt and nodecore.gametime >= entry.lpt then
		nodecore.log("action", string_format("%s at %s running at power %d level %d",
				anchorname, minetest.pos_to_string(pos), total, level))
		entry.lpt = nil
	end
	local cur = levels[level]
	local scale = get_cost_scale()
	if cur and total < (cur.qty * 0.4 * scale) and not entry.lpt then
		entry.lv = setlevel(pos, total, level, level - 1)
	else
		local nxt = levels[level + 1]
		nxt = nxt and level <= get_level_max() and nxt
		if nxt and total >= (nxt.qty * 0.9 * scale) then
			entry.lv = setlevel(pos, total, level, level + 1)
		end
	end

	return entry
end

minetest.register_lbm({
		name = modname .. ":loadprotect",
		run_at_every_load = true,
		nodenames = {anchorname},
		action = function(pos)
			local entry = nav(db, minetest.pos_to_string(pos))
			nodecore.log("action", string_format("%s at %s loaded at power %d level %d",
					anchorname, minetest.pos_to_string(pos), entry.qty or 0, entry.lv or 0))
			entry.lpt = nodecore.gametime + 20
		end
	})

local function optic_check(pos, _, recv)
	statecheck(pos, recv)
end

nodecore.interval(5, function()
		if nodecore.stasis or not nodecore.gametime then return end
		local keys = {}
		for k in pairs(db) do keys[k] = true end
		for k in pairs(keys) do statecheck(minetest.string_to_pos(k), nil, k) end
		modstore:set_string("db", minetest.serialize(db))
	end)

------------------------------------------------------------------------
-- NODE DEFINITION

local amalgam = minetest.registered_nodes["nc_igneous:amalgam"]

local txr_sides = "(nc_lode_annealed.png^[mask:nc_tote_sides.png)"
local txr_handle = "(nc_lode_annealed.png^nc_tote_knurl.png)"
local txr_top = txr_handle .. "^[transformFX^[mask:nc_tote_top.png^[transformR90^" .. txr_sides

minetest.register_node(anchorname, {
		description = "Force Lode",
		drawtype = "mesh",
		visual_scale = nodecore.z_fight_ratio,
		mesh = "nc_tote_handle.obj",
		paramtype = "light",
		paramtype2 = "facedir",
		tiles = {
			txr_sides,
			txr_sides,
			txr_top,
			txr_handle,
			amalgam.tiles[1]
		},
		backface_culling = true,
		use_texture_alpha = "clip",
		groups = {
			[modname] = 1,
			[modname .. "_melt"] = 1,
			snappy = 1,
			igniter = 1,
			stack_as_node = 1,
			falling_node = 1,
			damage_radiant = 4,
		},
		stack_max = 1,
		light_source = amalgam.light_source,
		crush_damage = 4,
		sounds = nodecore.sounds("nc_lode_annealed"),
		optic_check = optic_check,
		mapcolor = {r = 53, g = 43, b = 43},
	})

------------------------------------------------------------------------
-- CONSTRUCTION/DESTRUCTION

nodecore.register_craft({
		label = "assemble load anchor",
		normal = {x = 1},
		indexkeys = {"nc_igneous:amalgam"},
		nodes = {
			{match = "nc_igneous:amalgam", replace = "air"},
			{x = -1, match = "nc_tote:handle", replace = modname .. ":anchor"},
		}
	})

local function anchor_melt(pos)
	local rods = math_random(1, 3)
	nodecore.item_eject(pos, "nc_lode:rod_hot", 5, rods)
	local bars = math_random(1, 7 - 2 * rods)
	nodecore.item_eject(pos, "nc_lode:bar_hot", 5, bars)
	nodecore.item_eject(pos, "nc_lode:prill_hot", 5, 8 - 2 * rods - bars)
	return nodecore.set_loud(pos, {name = "nc_terrain:lava_source"})
end

minetest.register_abm({
		label = "load anchor melt",
		interval = 1,
		chance = 2,
		nodenames = {"group:" .. modname .. "_melt"},
		arealoaded = 1,
		action = function(pos)
			if nodecore.quenched(pos) then return end
			return anchor_melt(pos)
		end
	})

nodecore.register_aism({
		label = "load anchor stack melt",
		interval = 1,
		chance = 2,
		arealoaded = 1,
		itemnames = {"group:" .. modname .. "_melt"},
		action = function(stack, data)
			if nodecore.quenched(data.pos) then return end
			if stack:get_count() == 1 and data.node then
				local def = minetest.registered_nodes[data.node.name]
				if def and def.groups and def.groups.is_stack_only then
					anchor_melt(data.pos)
					stack:take_item(1)
					return stack
				end
			end
			for rel in nodecore.settlescan() do
				local p = vector.add(data.pos, rel)
				if nodecore.buildable_to(p) then
					anchor_melt(p)
					stack:take_item(1)
					return stack
				end
			end
		end
	})

------------------------------------------------------------------------
-- PARTICLE EFFECTS

local function intersect(mina, maxa, minb, maxb)
	if mina.x > maxb.x or minb.x > maxa.x
	or mina.y > maxb.y or minb.y > maxa.y
	or mina.z > maxb.z or minb.z > maxa.z
	then return end
	return {
		x = mina.x > minb.x and mina.x or minb.x,
		y = mina.y > minb.y and mina.y or minb.y,
		z = mina.z > minb.z and mina.z or minb.z,
		}, {
		x = maxa.x < maxb.x and maxa.x or maxb.x,
		y = maxa.y < maxb.y and maxa.y or maxb.y,
		z = maxa.z < maxb.z and maxa.z or maxb.z,
	}
end

local function paricle_clip(def)
	local rawmin = def.pos.min
	local rawmax = def.pos.max
	for _, player in ipairs(minetest.get_connected_players()) do
		local pos = vector.add(player:get_pos(), player:get_velocity())
		pos.y = pos.y + player:get_properties().eye_height
		local minp, maxp = intersect(rawmin, rawmax,
			{x = pos.x - 8, y = pos.y - 8, z = pos.z - 8},
			{x = pos.x + 8, y = pos.y + 8, z = pos.z + 8})
		if minp then
			def.pos.min = minp
			def.pos.max = maxp
			def.playername = player:get_player_name()
			minetest.add_particlespawner(def)
		end
	end
end

local particletime = 1
nodecore.register_dnt({
		name = modname .. " particles",
		time = particletime,
		nodenames = {anchorname},
		loop = true,
		autostart = true,
		action = function(pos)
			local lvnum = statecheck(pos).lv
			if (not lvnum) or lvnum < 1 then return end
			local level = levels[lvnum]
			if not level then return end

			local lvscale = (level.around + 0.5)
			minetest.add_particlespawner({
					time = particletime,
					amount = level.facearea * 2 * particletime,
					exptime = particletime,
					size = {
						min = 0.2 * lvnum,
						max = 0.5 * lvnum,
						bias = 1
					},
					collisiondetection = false,
					glow = 1,
					texture = "nc_forcelode_particle.png",
					pos = {
						min = vector.offset(pos, -0.5, -0.5, -0.5),
						max = vector.offset(pos, 0.5, 0.5, 0.5),
					},
					attract = {
						kind = "point",
						strength = {
							min = -1 * lvscale,
							max = -5 * lvscale,
							bias = 1
						},
						origin = pos
					}
				})

			local mbctr = {
				x = math_floor(pos.x / 16) * 16,
				y = math_floor(pos.y / 16) * 16,
				z = math_floor(pos.z / 16) * 16,
			}
			local function addwall(dxa, dya, dza, dxb, dyb, dzb)
				paricle_clip({
						time = particletime,
						amount = 50 * particletime,
						exptime = particletime,
						size = {
							min = 0.2 * lvnum,
							max = 0.5 * lvnum,
							bias = 1
						},
						collisiondetection = false,
						glow = 1,
						texture = "nc_forcelode_particle.png",
						pos = {
							min = {
								x = mbctr.x + dxa,
								y = mbctr.y + dya,
								z = mbctr.z + dza,
							},
							max = {
								x = mbctr.x + dxb,
								y = mbctr.y + dyb,
								z = mbctr.z + dzb,
							},
						},
						attract = {
							kind = "point",
							strength = {
								min = 0.01,
								max = 0.02,
								bias = 1
							},
							origin = pos
						}
					})
			end
			local n = level.around * -16 - 0.5
			local p = level.around * 16 + 15.5
			addwall(n, n, n, p, p, n)
			addwall(n, n, n, p, n, p)
			addwall(n, n, n, n, p, p)
			addwall(n, n, p, p, p, p)
			addwall(n, p, n, p, p, p)
			addwall(p, n, n, p, p, p)
		end
	})

------------------------------------------------------------------------
-- FORCE LOADING

local reqqty = 0
local forced = {}
local prevlimit
nodecore.interval(60, function()
		local blocks = {}
		for k, v in pairs(db) do
			local level = v.lv and v.lv > 0 and levels[v.lv]
			if level then
				local pos = minetest.string_to_pos(k)
				local bpos = {
					x = math_floor(pos.x / 16) * 16,
					y = math_floor(pos.y / 16) * 16,
					z = math_floor(pos.z / 16) * 16,
				}
				for dx = -level.around, level.around do
					for dy = -level.around, level.around do
						for dz = -level.around, level.around do
							local p = {
								x = bpos.x + dx * 16,
								y = bpos.y + dy * 16,
								z = bpos.z + dz * 16,
								p = v.lv + nodecore.boxmuller() * 2
								+ 1 / (dx * dx + dy * dy + dz * dz
									+ 0.01)
							}
							local pk = minetest.hash_node_position(p)
							local o = blocks[pk]
							if (not o) or (o.p < p.p) then
								p.k = pk
								blocks[pk] = p
							end
						end
					end
				end
			end
		end

		local list = {}
		for _, v in pairs(blocks) do list[#list + 1] = v end
		table_sort(list, function(a, b) return a.p > b.p end)
		reqqty = #list

		local added = 0
		local limit = get_block_limit()
		local max = limit > reqqty and reqqty or limit
		local keep = {}
		for i = 1, max do
			local pos = list[i]
			local res = forced[pos.k]
			if not res then
				res = minetest.forceload_block(pos, true, -1)
				if res then
					forced[pos.k] = pos
					added = added + 1
				end
			end
			if res then keep[pos.k] = true end
		end
		local rm = {}
		local removed = 0
		for k, v in pairs(forced) do
			if not keep[k] then rm[k] = v end
		end
		for k, v in pairs(rm) do
			minetest.forceload_free_block(v, true)
			forced[k] = nil
			removed = removed + 1
		end

		if added + removed == 0 and limit == prevlimit then return end
		prevlimit = limit
		nodecore.log("action", string_format("%s: requested %d, limit %d, enabled %d,"
				.. " added %d, removed %d", modname, reqqty, limit, max, added, removed))
	end)

------------------------------------------------------------------------
-- CHAT COMMANDS

minetest.register_chatcommand(modname, {
		description = "Dump global info about ForceLodes",
		privs = {["debug"] = true},
		func = function()
			local t = {}

			local s = {}
			local lvs = {}
			for k, v in pairs(db) do
				local pos = minetest.string_to_pos(k)
				local l = v.lv or 0
				lvs[l] = (lvs[l] or 0) + 1
				s[#s + 1] = string_format("%s->%s",
					minetest.pos_to_string(pos), l)
			end
			t[#t + 1] = "forcelodes: "
			t[#t + 1] = table_concat(s, " ")
			local keys = {}
			for k in pairs(lvs) do keys[#keys + 1] = k end
			table_sort(keys)
			local totals
			for _, k in ipairs(keys) do
				if not totals then
					t[#t + 1] = "; total"
					totals = true
				end
				t[#t + 1] = string_format(" lv%d=%d", k, lvs[k])
			end
			t[#t + 1] = "\n"

			local q = 0
			for _ in pairs(forced) do q = q + 1 end
			t[#t + 1] = string_format("mapblocks: requested %d, limit %d, active %d\n",
				reqqty, get_block_limit(), q)

			return true, table_concat(t)
		end
	})
